<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# Forward Networks Create Network Collection

## Table of Contents

* [Overview](#overview)
  * [Operations Manager and JSON-Form](#operations-manager-and-json-form)
* [Requirements](#requirements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
  * [Input Variables](#input-variables)
* [Additional Information](#additional-information)

## Overview

The Forward Networks Create Network Collection pre-built can be used to do service verfication before a deployment. The pre-built consists of transformations and childjobs that makes the pre-built modular in design and generic to verify any type of service using [Forward Enterprise](https://forwardnetworks.com).

The main parent flow which is shown in the overview contains functionality for:
1. Forward Networks Create Network Collection
2. JST : JSON Schema Transformation
<table><tr><td>
  <img src="./images/forward_networks_create_network_collection_autocatalog.png" alt="workflow" width="800px">
</td></tr></table>
<table><tr><td>
  <img src="./images/forward_networks_create_network_collection_automation.png" alt="workflow" width="800px">
</td></tr></table>

_Estimated Run Time_: 1min

## Installation Prerequisites

Users must satisfy the following pre-requisites:

* Itential Automation Platform
  * `^2021.2`
  * [Forward Networks Adapter](https://gitlab.com/itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks)

## Requirements

This Pre-Built requires the following:

* [Forward Networks Adapter](https://gitlab.com/itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks) configured with a Forward Enterprise instance.

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).


## How to Run

Use the following to run the Pre-Built:

* Run the Operations Manager item `Forward Networks Create Network Collection` or call [Forward Networks Create Network Collection](./bundles/workflows/Forward%20Networks%20Create%20FNetwork%20Collection.json) from your workflow as a child job.

### Input Variables
_Example_

```json
{
  "networkId": "Unique Identifier Associated with Network within Forward Enterprise"
}
```
